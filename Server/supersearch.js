//select s. name, string_agg(t.type, ', '), address, city, zip from stores s join types t on s.store_id = t.store_id group by s.name, s.address, s.city, s.zip order by s.name;
const express = require('express');
var Pool = require('pg').Pool;
var bodyParser = require('body-parser');
var Bcrypt = require('bcrypt');

const app = express();
var config = {
	host: 'localhost',
	user: 'searchmanager',
	password:'Nutrient17',
	database:'supersearch',
};

var pool = new Pool(config);

app.set('port',(3001));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(function(req,res,next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	next();
})

if(process.env.NODE_ENV === 'production') {
	console.log('Running production server.');
	app.use(express.static('../build'));
}


app.get('/search', async (req, res) => {
	console.log(req.query.hit);
	var hit = req.query.hit;
	var zip = req.query.zip;
	let result = null;
	try {
		if(hit) {
			if(!zip) {
				console.log(zip);
				if (hit === 'movie' || hit === 'movies') {
					var responseM = await pool.query("select * from movies");
					result = responseM.rows.map(function(item){
						return {movie: item.movie, theater: item.theater, address: item.address, city: item.city, zip: item.zip}
					})
				} else {
					var responseM = await pool.query("select * from movies where movie like $1", ['%'+hit+'%']);
					if(responseM.rows.length === 0) {
						var response1 = await pool.query("select s.name, string_agg(t.type, ', ') as types, address, city, zip from stores s join types t on s.store_id = t.store_id where s.name like $1 or t.type like $1 group by s.name, s.address, s.city, s.zip order by s.name limit 25;",['%'+hit+'%']);
						console.log(response1.rows);
						result = response1.rows.map(function(item){
						return {name: item.name, types: item.types, address: item.address, city: item.city, zip: item.zip}
						})
					} else {
						result = responseM.rows.map(function(item){
						return {movie: item.movie, theater: item.theater, address: item.address, city: item.city, zip: item.zip}
						})
					}
				}
			} else {
				console.log(zip);
				if (hit === 'movie' || hit === 'movies') {
					var responseM = await pool.query("select * from movies where zip = $1", [zip]);
					result = responseM.rows.map(function(item){
						return {movie: item.movie, theater: item.theater, address: item.address, city: item.city, zip: item.zip}
					})
				} else {
					var responseM = await pool.query("select * from movies where movie like $1 and zip = $2", ['%'+hit+'%', zip]);
				if(responseM.rows.length === 0) {
					var response2 = await pool.query("select s.name, string_agg(t.type, ', ') as types, address, city, zip from stores s join types t on s.store_id = t.store_id where (s.name like $1 or t.type like $1) and zip = $2 group by s.name, s.address, s.city, s.zip order by s.name limit 25;",['%'+hit+'%', zip]);
					console.log(response2.rows);
					result = response2.rows.map(function(item){
					return {name: item.name, types: item.types, address: item.address, city: item.city, zip: item.zip}
					})
				} else {
					result = responseM.rows.map(function(item){
					return {movie: item.movie, theater: item.theater, address: item.address, city: item.city, zip: item.zip}
					})
				}
				}
			}
			//console.log(result);
			res.json ({table: result});
		}
	}
	catch(e) {
	console.error('Error running query ' +e);
	}
});

async function execute_query(query, args) {
	try {
			var response = await pool.query(query, args);
	} catch(e) {
		console.error(e);
	}
}

function add_user(username, password, zip) {
	Bcrypt.hash(password, 10, function(err, hash) {
		execute_query("insert into users values ($1,$2,$3)",[username, hash, zip]);
	})
}

app.post('/signup', async (req, res) => {
	console.log(req.body);
	console.log('hello');
	var usnm = req.body.username;
	var pass = req.body.password;
	var zip = req.body.zip;
	try {
		if(!usnm || !pass || !zip) {
			res.json({status: 'parameters not given or missing'});
		} else {
			var response = await pool.query('select * from users where username = $1',[usnm]);
			if (response.rows.length > 0) {
				res.json ({status: 'Username Taken'});
			}
			else {
				add_user(usnm,pass,zip);
				res.json({status: 'User Added'});
			}
		}
	}
	catch(e) {
		console.error('Error running insert ' +e);
	}
});

async function login (hash, password,res, response) {
	Bcrypt.compare(password, hash, function (err,resp) {
			if(resp) {
				let result = response.rows.map(function(item){
					return {username:item.username, zip:item.zipcode}
				})
				res.json({info: result});
			} else {
				res.json({info: 'not match'});
			}
		});
}

app.get('/login', async (req, res) => {
	var username = req.query.username;
	var password = req.query.password;
	var response = await pool.query('select username, hash, zipcode from users where username = $1', [username]);
	var hash = response.rows[0].hash;
	let result = [];
	if(response.rows.length === 0){
		res.json({status: 'User info does not match'})
	} else {
		login(hash,password,res,response);
	}
})

app.listen(app.get('port'), () => {
	console.log('Running');
})
