create database supersearch;
\c supersearch;

create table movies (
	movie varchar(255),
	theater varchar(255),
	address varchar(255),
	city varchar(255),
	zip varchar(5)
	);
--
-- data for table movies

INSERT INTO movies (movie, theater, address, city, zip) VALUES
('Nobody''s Watching', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('It', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('The Limehouse Golem', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('Despicable Me 3', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('Wonder Woman', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('The Emoji Movie', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('Year By The Sea', 'Marquee Cinemas Southpoint 9', '5800 South Point Centre', 'Fredericksburg',  22401),
('Rememory', 'Allen Cinema 4 Mesilla Valley', '700 South Telshor Boulevard', 'Las Cruces', 88005),
('Wonder Woman', 'Allen Cinema 4 Mesilla Valley', '700 South Telshor Boulevard', 'Las Cruces', 88005),
('Dunkirk', 'Allen Cinema 4 Mesilla Valley', '700 South Telshor Boulevard', 'Las Cruces', 88005),
('Anti Matter', 'Allen Cinema 4 Mesilla Valley', '700 South Telshor Boulevard', 'Las Cruces', 88005);

-- --------------------------------------------------------

create table stores (
	store_id serial primary key ,
	name varchar(255),
	address varchar(255),
	city varchar(255),
	zip varchar(5)
	);
--
-- Data for table stores
--

INSERT INTO stores (name, address, city, zip) VALUES
('Hyperion Espresso', '301 William St.',  'Fredericksburg', 22401),
('Starbucks', '2001 Plank Road', 'Fredericksburg', 22401),
('25 30 Expresso', '400 Princess Anne St', 'Fredericksburg', 22401),
('Agora Downtown', '520 Caroline St', 'Fredericksburg', 22401),
('Highpoint Coffee', '615 Caroline St', 'Fredericksburg', 22401),
('Duck Donuts', '1223 Jefferson Davis Hwy', 'Fredericksburg', 22401),
('Basilico', '2577 Cowan Blvd', 'Fredericksburg',  22401),
('Cork and Table', '909 Caroline', 'Fredericksburg',  22401),
('Orofino', '1251 Carl D Silver Parkway', 'Fredericksburg',  22401),
('Pancho Villa Mexican Rest', '10500 Spotsylvania Ave', 'Fredericksburg', 22401),
('Chipotle', '5955 Kingstowne', 'Fredericksburg', 22401),
('Sedona Taphouse', '591 Williams', 'Fredericksburg', 22401),
('Pueblo''s Tex Mex Grill', '1320 Jefferson Davis Hwy', 'Fredericksburg', 22401),
('El Pino', '4211 Plank Road', 'Fredericksburg', 22401),
('Starbucks', '2808 Telshor Blvd', 'Las Cruces', 88005),
('Starbucks', '2511 Lohman Ave', 'Las Cruces', 88005),
('Milagro Coffee Y Espresso', '1733 E. University Ave', 'Las Cruces', 88005),
('Starbucks', '1500 South Valley',  'Las Cruces', 88005),
('Bean', '2011 Avenida De Mesilla',  'Las Cruces', 88005),
('El Comedor', '2190 Avenida De  Mesilla', 'Las Cruces', 88005),
('Los Compas', '603 S Nevarez St.',  'Las Cruces', 88005),
('La Fuente', '1710 S Espina',  'Las Cruces', 88005),
('La Posta', '2410 Calle De San Albino',  'Las Cruces', 88005),
('El Jacalito', '2215 Missouri Ave',  'Las Cruces', 88005),
('Peet''s', '2260 Locust',  'Las Cruces', 88005);

-- --------------------------------------------------------

create table types (
	type_id serial primary key,
	store_id integer references stores(store_id),
	type varchar(255)
	);

insert into types (store_id,type) VALUES
(1, 'cafe'),
(1, 'coffee'),
(2, 'coffee'),
(2, 'cafe'),
(2, 'pastry'),
(3, 'cafe'),
(3, 'coffee'),
(4, 'coffee'),
(5, 'coffee'),
(6, 'cafe'),
(6, 'coffee'),
(6, 'donut'),
(7, 'italian'),
(7, 'restaurant'),
(8, 'american'),
(8, 'restaurant'),
(9, 'italian'),
(9, 'restaurant'),
(10, 'mexican'),
(10, 'restaurant'),
(11, 'mexican'),
(11, 'restaurant'),
(12, 'american'),
(12, 'restaurant'),
(13, 'mexican'),
(13, 'restaurant'),
(14, 'mexican'),
(14, 'restaurant'),
(15, 'cafe'),
(15, 'coffee'),
(15, 'pastry'),
(16, 'cafe'),
(16, 'coffee'),
(16, 'pastry'),
(17, 'coffee'),
(17, 'cafe'),
(18, 'cafe'),
(18, 'coffee'),
(18, 'pastry'),
(19, 'coffee'),
(20, 'mexican'),
(20, 'restaurant'),
(21, 'mexican'),
(21, 'restaurant'),
(22, 'mexican'),
(22, 'restaurant'),
(23, 'mexican'),
(23, 'restaurant'),
(24, 'mexican'),
(24, 'restaurant'),
(25, 'coffee');

create table users (
	username varchar(13),
	hash varchar(256),
	zipcode varchar(5)
	);