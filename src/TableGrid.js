var PropTypes = require('prop-types');
var React = require('react');
var Content = require('./Content').default;

class TableGrid extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
      table: null
     };
 }
 render (){
 	console.log(this.props.table.table);
 	return (
 		<table>
 		<thead>
 			<tr>
 				{this.props.table.table.length === 0? null : !this.props.table.table[0].movie?null : <th>Movie</th> }
 				{this.props.table.table.length === 0? null : !this.props.table.table[0].name? null :<th>Name</th>}
 				{this.props.table.table.length === 0? null : !this.props.table.table[0].theater? null :<th>Theater</th>}
 				{this.props.table.table.length === 0? null : !this.props.table.table[0].types? null :<th>Types</th>}
 			  	{this.props.table.table.length === 0? null :<th>Address</th>}
				{this.props.table.table.length === 0? null :<th>City</th>}
				{this.props.table.table.length === 0? null :<th>Zipcode</th>}
 			</tr>
 			</thead>
 			<tbody className = 'tabdiv'>
 			{
 				this.props.table.table.map((data) => {
 					return <Content data= {data} />

 				})
 			}
 			</tbody>
 		</table>
 	)
 }
}

TableGrid.PropTypes = {
	table: PropTypes.array.isRequire,
}

export default TableGrid;