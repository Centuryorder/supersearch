var React = require('react');
var Axios = require('axios');
var TableGrid = require('./TableGrid').default ;

function getData(hit, zip) {
  return Axios.get('http://35.193.40.242/search?hit=' + hit+'&zip=' + zip);
  //eturn Axios.get('http://localhost:3001/search?hit=' + hit+'&zip=' + zip);
}

function getTable(hit,zip) {
  return Axios.all([
    getData(hit,zip)]).then(function (data) {
      //console.log(data[0].data.table);
      //console.log(t);
      var tab = data[0].data.table;
      return {
        table: tab
      }
    });
}

function handleError (error) {
  console.warn(error);
  return null;
}
  
  class Search extends React.Component {
     constructor(props) {
      super(props);
      this.state = {
        hit: '',
        username: '',
        zipcode:'',
        table: null
      };

      this.handleChange = this.handleChange.bind(this);
    } 

    componentWillMount() {
   
      this.setState({
        username: this.props.username,
        zipcode: this.props.zipcode
      });
    }
   handleChange(event) {
      var value = event.target.value;
      if(value === '') {
        this.setState({
          table: null,
          hit: value
        })
      }
      this.setState({
          hit: value
        })
      getTable(value,this.state.zipcode).then(res => {
        //console.log(res);
        this.setState({
          table: res
        })
      });
    }
    render() {
     return (
        <div >
          <p className="App-intro">
            Food Search
          </p>
          <p><input type="text" 
              className="rounded" 
              placeholder="Search foods..."
              value={this.state.searchTerm}
              onChange={this.handleChange}  />
          </p>
          <div>
          {!this.state.table?<p></p>:<TableGrid table={this.state.table}/>}
          </div>
        </div>
      );
    }
  }
  
  module.exports  = Search;