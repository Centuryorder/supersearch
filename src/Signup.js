var React = require('react');
var PropTypes = require('prop-types');
var Axios = require('axios');
Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
var Link = require('react-router-dom').Link;


class Signup extends React.Component {

    constructor(props){
      super(props);
      this.state = {
        username: '',
        password: '',
        zipcode: '',
        response: ''
      } 
      this.handleChange = this.handleChange.bind(this);
      this.handlePasswordChange = this.handlePasswordChange.bind(this);
      this.handleZipcodeChange = this.handleZipcodeChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          username: value
        }
      })
    }
    handleZipcodeChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          zipcode: value
        }
      })
    }

    handlePasswordChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          password: value
        }
      })
    }

    handleSubmit(event){
      event.preventDefault();
      //postData(this.state.username, this.state.password, this.state.zipcode);
      Axios.post('http://35.193.40.242/signup', {
      //Axios.post('http://localhost:3001/signup', {
        username: this.state.username,
        password: this.state.password,
        zip: this.state.zipcode
      }).then(data => {
        var resp = data.data.status;
        this.setState(function(){
          return {
            response : resp
          }
        })
      });
    }



    render(){
        if (!this.props.username){
        return (<div><h2>Sign Up</h2>
        <form className='column' onSubmit={this.handleSubmit.bind(this)}>
        <p>
        
        <input
          id='username'
          className="rounded" 
          placeholder='username'
          type='text'
          autoComplete='off'
          value = {this.state.username}
          onChange={this.handleChange}
        /></p>

        <p>
        <input
          id='password'
          className="rounded" 
          type='password'
          placeholder='password'
          autoComplete='off'
          value = {this.state.password}
          onChange={this.handlePasswordChange}
        /></p>


        <p>
        <input
          id='zipcode'
          className="rounded" 
          placeholder='Zipcode'
          type='text'
          autoComplete='off'
          value = {this.state.zipcode}
          onChange={this.handleZipcodeChange}
        /></p>

        <button
          className="button" 
          type='submit'
          disabled={!this.state.username || !this.state.zipcode || !this.state.password}
           >
            Sign Up
        </button>
      </form>
            <h2>{!this.state.response?'': this.state.response}</h2>
            {!this.state.response === 'User Added'?<p><Link to={{pathname: '/login'}}> Log In here </Link></p>:<p></p>}
        </div>
    )
  }
  else {
    return (
      <h2>{!this.state.response?'': this.state.response}</h2>
    )
  }
  }

}

module.exports = Signup;