var PropTypes = require('prop-types');
var React = require('react');

class Content extends React.Component {
	constructor(props) {
    super(props);
	this.state = {
		data: null
	     };
	}
	render () {
		return (
			<tr>
				{!this.props.data.movie? null :<td className>{this.props.data.movie}</td>}
				{!this.props.data.name? null :<td className>{this.props.data.name}</td>}
				{!this.props.data.theater? null :<td className>{this.props.data.theater}</td>}
				{!this.props.data.types? null :<td className>{this.props.data.types}</td>}
				<td className>{this.props.data.address}</td>
				<td className>{this.props.data.city}</td>
				<td className>{this.props.data.zip}</td>
			</tr>
		)
	}
}

export default Content;